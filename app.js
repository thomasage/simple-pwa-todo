if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('/service-worker.js', {scope: '/'})
        .then(registration => {
            console.log('Registration succeeded. Scope is ' + registration.scope);
        })
        .catch(error => {
            console.log('Registration failed with ' + error);
        })
}

const CACHE_VERSION = 'v1'

document.getElementById('action-drop-cache').addEventListener('click', dropCache)
document.getElementById('action-load-tasks').addEventListener('click', loadTasks)

const tasksList = document.getElementById('tasks')

async function loadTasks(event) {
    event.preventDefault()
    console.debug('loadTasks')
    const request = '/tasks.json'
    await loadTasksFromCache(request)
    await loadTasksFromNetwork(request)
}

function dropCache(event) {
    event.preventDefault()
    caches.keys().then(names => {
        for (let name of names) {
            caches.delete(name)
        }
    })
    console.debug('Cache dropped')
}

async function loadTasksFromCache(request) {
    console.debug('loadTasksFromCache')
    const response = await caches.match(request)
    if (!response) {
        return
    }
    const payload = await response.json()
    renderTasksList(payload)
}

async function loadTasksFromNetwork(request) {
    console.debug('loadTasksFromNetwork')
    try {
        const response = await fetch(request)
        await saveTasks(request, response.clone())
        const payload = await response.json()
        renderTasksList(payload)
    } catch (error) {
        console.debug('No connection')
    }
}

async function saveTasks(request, response) {
    const cache = await caches.open(CACHE_VERSION)
    await cache.put(request, response)
}

function renderTasksList(payload) {
    console.debug('renderTasksList')
    const tasksList = document.getElementById('tasks')
    tasksList.innerHTML = ''
    payload.forEach(item => {
        tasksList.innerHTML += `<div>#${item.id} &ndash; ${item.title}</div>`
    })
}
