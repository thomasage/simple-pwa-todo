const URLS = [
    '/',
    '/app.css',
    '/app.js',
    '/favicon.ico'
]
const CACHE_VERSION = 'v1'

this.addEventListener('install', event => {
    console.log('install')
    event.waitUntil(
        caches.open(CACHE_VERSION)
            .then(cache => cache.addAll(URLS))
    )
})

this.addEventListener('fetch', async event => {
    const url = new URL(event.request.url);
    if (URLS.indexOf(url.pathname) === -1) {
        return
    }
    console.debug('Fetch', event.request.url)
    event.respondWith(
        fetchCacheFirst(event.request)
    )
})

async function fetchCacheFirst(request) {
    let response = await caches.match(request)
    if (response) {
        console.debug(request.url, 'from cache 👍')
    } else {
        console.debug(request.url, 'from cache 👎')
        response = await fetch(request)
        if (response) {
            console.debug(request.url, 'from network 👍')
            const cache = caches.open(CACHE_VERSION)
            cache.put(request, response)
        } else {
            console.debug(request.url, 'from network 👎')
            response = new Response('EMPTY')
        }
    }
    return response
}
